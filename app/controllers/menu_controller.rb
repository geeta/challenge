class MenuController < ApplicationController

  require 'json'
  require 'open-uri'
  ROMAN_NUMERALS = ['','I','II','III','IV','V','VI','VII','VIII','IX', 'X']

  def index
  end

 
  # GET /menu/donkey_kong
  # "Print out numbers from 1 to 100. When the number is divisible by 
  # three, write 'Donkey' instead of the number. If it is divisible 
  # by five, write 'Kong' instead of the number. For multiples of 3 
  # and 5, write 'DonkeyKong'."
  def donkey_kong
    @results = (1..100).map{|i|
      if (i % 3 == 0) && (i % 5 == 0)
        "DonkeyKong"
      elsif (i % 3 == 0)
        "Donkey"
      elsif (i % 5 == 0)
        "Kong"
      else
        i.to_s
      end
    }

    respond_to do |format|
      format.html # donkey_kong.html.erb
      format.json { render :json => @results }
    end 
  end


  # GET /menu/roman_numerals
  # "Write a program that lists out numbers from 1 to 100 and their 
  # roman numeral equivalent values. (Assume 4 is 'IV' not 'IIII')"
  def roman_numerals
      @results = (1..100).map{|i|
      roman_i = '' 
      num = i
      if num > 9
          roman_i = (1..(num / 10)).map{|n| ROMAN_NUMERALS[10]}.join('')
      end
      roman_i = roman_i + ROMAN_NUMERALS[num % 10]
      i.to_s + " -> " + roman_i
    }

    respond_to do |format|
      format.html # roman_numerals.html.erb
      format.json { render :json => @results }
    end   
  end


  # GET /menu/app_net
  # "Consume the public feed and print out a simple list of posts 
  # displaying username & message."
  # Note: I'm only consuming the first 50 pages
  def app_net
    catch(:over) do
      last_page = false
      num_pages = 0
      prev_max_id = 1
      @results = []
      until last_page == true do   
        #only consuming first 50 pages, in case there are too many results   
        num_pages += 1
        throw :over if (num_pages > 50)
        
        #get data from app.net for the current page
        if num_pages == 1
          response = JSON.parse(open('https://alpha-api.app.net/stream/0/posts/stream/global?count=200').read)
        else
          response = JSON.parse(open('https://alpha-api.app.net/stream/0/posts/stream/global?count=200&since_id=' + prev_max_id).read)
        end
        data = response["data"]

        #extract each item's username and post
        @results +=  data.map{|result| 
          username = result["user"]["username"]
          post = (result["user"]["description"] != nil)? result["user"]["description"]["text"] : "-"
          username + ' : ' + post
        }

        #check if there is still more data to pull from app.net
        if response["meta"]["more"] == false
          last_page = true
        end

        #get the max id of the current page, to use as the since_id for the next page's request
        prev_max_id = response["meta"]["max_id"]
 
      end #until
    end #catch

    respond_to do |format|
      format.html # app_net.html.erb
      format.json { render :json => @results }
    end
  end

  # GET /menu/github_rockstars
  # "Consume the Github API and find out who were the most active
  # contributors to Rails by month for the past 6 months. Should
  # display top 3 for each months."
  def github_rockstars
    @results = nil

    respond_to do |format|
      format.html # github_rockstars.html.erb
      format.json { render :json => @results }
    end
  end

end
